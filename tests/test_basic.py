import pytest
from django.conf import settings


class TestBasic(object):

    # just a passing test to report
    def test_test_framework(self):
        assert True

    # prove django settings is in da house
    def test_settings_inclusion(self):
        assert settings.SETTINGS_TEST_STRING == 'XYZPDQ'

