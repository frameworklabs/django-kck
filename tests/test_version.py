import copy
import datetime
import pytest
import logging

from django_kck.cache import VersionMismatch
from django_kck.test import BaseCacheTest

logger = logging.getLogger(__name__)


class TestCacheKeyVersions(BaseCacheTest):
    VERSION_MATCH_PARAMS = dict(
        key='street_names/1',
        value=['Main', 'Market', 'Bridge'],
        hard_expire=datetime.datetime.utcnow() - datetime.timedelta(seconds=10),
        soft_expire=datetime.datetime.utcnow() - datetime.timedelta(seconds=30),
        modified=datetime.datetime.utcnow() - datetime.timedelta(seconds=3),
        primer_name='street_names',
        version=1)

    def test_version_match(self, f_cache):
        cache = f_cache()
        self.simulate_cache_entry(cache.cache_entry(**self.VERSION_MATCH_PARAMS))
        new_cache_entry_params = {**copy.copy(self.VERSION_MATCH_PARAMS),
                                  **dict(value=['Commerce', 'Bigby'], version=2)}
        result = cache.set(check_version=1, **new_cache_entry_params)
        assert result == new_cache_entry_params

    def test_version_mismatch(self, f_cache):
        cache = f_cache()
        self.simulate_cache_entry(cache.cache_entry(**self.VERSION_MATCH_PARAMS))
        new_cache_entry_params = {**copy.copy(self.VERSION_MATCH_PARAMS),
                                  **dict(value=['Commerce', 'Bigby'], version=2)}

        with pytest.raises(VersionMismatch):
            cache.set(check_version=99, **new_cache_entry_params)
