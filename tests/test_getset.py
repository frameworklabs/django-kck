import pytest
from django_kck.test import BaseCacheTest


class TestGetSet(BaseCacheTest):

    def test_setget(self, f_cache):
        cache = f_cache()
        key = 'somekey/1'
        val = 'someval'
        assert not cache.is_set(key)
        cache.set(key, val)
        assert cache.is_set(key)
        assert cache.get(key)['value'] == val
