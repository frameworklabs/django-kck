import pytest
from unittest.mock import patch
from django_kck.test import BaseCacheTest


class TestPrimeOnCacheMiss(BaseCacheTest):

    def test_prime_on_cache_miss_primer_class_attribute(self, f_cache):
        with patch('app_with_primers1.primers.StreetNamesPrimer.compute') as mock_street_names_compute:
            with patch('app_with_primers2.primers.CustomerNamesPrimer.compute') as mock_customer_names_compute:
                cache = f_cache()
                # street names primer has prime_on_cache_miss=True, test compute() is called
                mock_street_names_compute.return_value = "DOES NOT MATTER WHAT I SET THIS TO"
                assert mock_street_names_compute.call_count == 0
                try:
                    cache.get('street_names/1')
                except Exception:
                    pass
                assert mock_street_names_compute.call_count == 1

                # customer names primer has prime_on_cache_miss=False, test compute() is not called
                try:
                    cache.get('customer_names/1')
                except Exception:
                    pass
                assert mock_customer_names_compute.call_count == 0

    def test_prime_on_cache_miss_get_argument(self, f_cache):
        with patch('app_with_primers2.primers.CustomerNamesPrimer.compute') as mock_customer_names_compute:
            cache = f_cache()

            # customer names primer has prime_on_cache_miss=False, test compute() is not called
            try:
                cache.get('customer_names/1')
            except Exception:
                pass
            assert mock_customer_names_compute.call_count == 0

            # test that compute() is called, when get() called with prime_on_cache_miss=True
            try:
                cache.get('customer_names/1', prime_on_cache_miss=True)
            except Exception:
                pass
            assert mock_customer_names_compute.call_count == 1

