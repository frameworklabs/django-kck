import pytest
from django_kck.test import BaseCacheTest
from unittest import mock


class BaseCacheHooksTest(BaseCacheTest):
    hook_trigger = 'set'

    def test_hook(self, f_cache):

        with mock.patch('app_with_primers1.primers.mockable_function') as mock_mockable_function:
            cache = f_cache()

            if self.hook_trigger == 'set':
                cache.set(self.key, 'somevalue')
                assert mock_mockable_function.call_count == self.expected_call_count
            elif self.hook_trigger == 'prime':
                cache.get(self.key, prime_on_cache_miss=True)


class TestSinglePreSetHook(BaseCacheHooksTest):
    key = 'onepresethook'
    expected_call_count = 1


class TestTwoPreSetHooks(BaseCacheHooksTest):
    key = 'twopresethooks'
    expected_call_count = 2


class TestSinglePostSetHook(BaseCacheHooksTest):
    key = 'onepostsethook'
    expected_call_count = 1


class TestTwoPostSetHooks(BaseCacheHooksTest):
    key = 'twopostsethooks'
    expected_call_count = 2


class BasePrimeHookTest(BaseCacheHooksTest):
    hook_trigger = 'prime'


class TestSinglePrePrimeHook(BasePrimeHookTest):
    key = 'onepreprimehook'
    expected_call_count = 1


class TestTwoPrePrimeHooks(BasePrimeHookTest):
    key = 'twopreprimehooks'
    expected_call_count = 2


class TestSinglePostPrimeHook(BasePrimeHookTest):
    key = 'onepostprimehook'
    expected_call_count = 1


class TestTwoPostPrimeHooks(BasePrimeHookTest):
    key = 'twopostprimehooks'
    expected_call_count = 2
