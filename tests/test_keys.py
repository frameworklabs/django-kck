import pytest
from django_kck.test import BaseCacheTest


class TestCacheBuildKey(BaseCacheTest):
    def test_build_key(self, f_cache):
        cache_obj = f_cache()
        built_key = cache_obj.build_key('string_and_int_parameters', id=123, name='Homer')
        assert built_key == 'string_and_int_parameters/123/Homer'


class TestPrimerParameterDict(BaseCacheTest):
    def test_parameter_dict(self, f_cache):
        cache_obj = f_cache()
        key = 'string_and_int_parameters/1/tom'
        primer_cls = cache_obj.primer(key)
        primer_obj = primer_cls(key)
        p = primer_obj.parameter_dict()
        assert p['id'] == 1
        assert p['name'] == 'tom'
