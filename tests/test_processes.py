import pytest
import datetime
from django_kck.test import BaseCacheTest
from unittest import mock


class TestProcesses(BaseCacheTest):
    def test_process_runs(self, f_cache):
        with mock.patch('app_with_primers1.primers.mockable_function') as mock_mockable_function:
            cache = f_cache()
            assert mock_mockable_function.call_count == 0
            cache.run_processes()
            assert mock_mockable_function.call_count == 1

    def test_interval_refresh(self, f_cache):

        # simulate existing cache entries
        cache = f_cache()
        time1 = datetime.datetime.utcnow() - datetime.timedelta(seconds=60)
        self.simulate_cache_entry(
            cache.cache_entry(
                key='interval_refresh/1',
                value=[time1],
                primer_name='interval_refresh',
                version=1))
        self.simulate_cache_entry(
            cache.cache_entry(
                key='interval_refresh/123',
                value=[time1],
                primer_name='interval_refresh',
                version=1))

        self.assert_current_cache_entry_value(cache, 'interval_refresh/1', [time1])
        self.assert_current_cache_entry_value(cache, 'interval_refresh/123', [time1])

