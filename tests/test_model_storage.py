import pytest

from django_kck.factories import DataProductFactory, RefreshQueueFactory, DataProduct, RefreshQueue


@pytest.mark.django_db
class TestModelStorage(object):
    def test_data_product(self):
        data_product = DataProductFactory()
        assert DataProduct.objects.all()[0].key == data_product.key

    def test_refresh_queue(self):
        refresh_queue_entry = RefreshQueueFactory()
        assert RefreshQueue.objects.all()[0].key == refresh_queue_entry.key
