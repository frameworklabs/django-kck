import datetime
import pytest

from django_kck.models import RefreshQueue
from django_kck.test import BaseCacheTest
from unittest import mock


class TestGetRespectsExpires(BaseCacheTest):

    def test_hard_expired_raises_key_error(self, f_cache):
        cache = f_cache()
        self.simulate_cache_entry(
            cache.cache_entry(
                key='street_names/1',
                value=['Main', 'Market', 'Bridge'],
                hard_expire=datetime.datetime.utcnow() - datetime.timedelta(seconds=2),
                modified=datetime.datetime.utcnow() - datetime.timedelta(seconds=3),
                primer_name='street_names',
                version=1))
        with pytest.raises(KeyError):
            cache.get('street_names/1', prime_on_cache_miss=False)

    def test_soft_expires_requests_refresh(self, f_cache):
        cache = f_cache()
        self.simulate_cache_entry(
            cache.cache_entry(
                key='street_names/1',
                value=['Main', 'Market', 'Bridge'],
                hard_expire=datetime.datetime.utcnow() + datetime.timedelta(seconds=2),
                soft_expire=datetime.datetime.utcnow() - datetime.timedelta(seconds=1),
                modified=datetime.datetime.utcnow() - datetime.timedelta(seconds=3),
                primer_name='street_names',
                version=1))
        assert RefreshQueue.objects.all().count() == 0
        cache_entry = cache.get('street_names/1', prime_on_cache_miss=False)
        assert RefreshQueue.objects.all().count() == 1
        assert cache_entry['value'] == ['Main', 'Market', 'Bridge']


class TestPrimeSetsExpiration(BaseCacheTest):

    def is_believably_close(self, ts, seconds):
        if abs((ts - datetime.datetime.utcnow()).total_seconds() - seconds) < 1:
            return True
        return False

    def test_hard_expire(self, f_cache):
        cache = f_cache()
        with pytest.raises(KeyError):
            cache.get('hard_expire/1', prime_on_cache_miss=False)

        cache_entry = cache.get('hard_expire/1', prime_on_cache_miss=True)
        assert self.is_believably_close(cache_entry['hard_expire'], 30)
        assert cache_entry['soft_expire'] is None

    def test_no_expire(self, f_cache):
        cache = f_cache()
        with pytest.raises(KeyError):
            cache.get('no_expire/1', prime_on_cache_miss=False)

        cache_entry = cache.get('no_expire/1', prime_on_cache_miss=True)
        assert cache_entry['hard_expire'] is None
        assert cache_entry['soft_expire'] is None

    def test_soft_expire(self, f_cache):
        cache = f_cache()
        with pytest.raises(KeyError):
            cache.get('soft_expire/1', prime_on_cache_miss=False)

        cache_entry = cache.get('soft_expire/1', prime_on_cache_miss=True)
        assert cache_entry['hard_expire'] is None
        assert self.is_believably_close(cache_entry['soft_expire'], 60)

    def test_both_hard_and_soft_expire(self, f_cache):
        cache = f_cache()
        with pytest.raises(KeyError):
            cache.get('hard_and_soft_expire/1', prime_on_cache_miss=False)

        cache_entry = cache.get('hard_and_soft_expire/1', prime_on_cache_miss=True)
        assert self.is_believably_close(cache_entry['hard_expire'], 45)
        assert self.is_believably_close(cache_entry['soft_expire'], 15)
