#!/bin/sh

# Remove old files
rm -f celerybeat-schedule celerybeat.pid

# run scheduler celery worker
su -m celeryuser -c "celery -A kck.celery beat"
