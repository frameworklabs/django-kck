from django_kck.cache import Primer


class CustomerNamesPrimer(Primer):
    key_base = 'customer_names'

    def compute(self):
        return ['Larry', 'Homer', 'Carl']
