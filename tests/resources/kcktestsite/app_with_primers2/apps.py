from django.apps import AppConfig


class AppWithPrimers2Config(AppConfig):
    name = 'app_with_primers2'
