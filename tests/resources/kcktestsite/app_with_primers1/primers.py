import logging

import datetime

from django_kck.cache import Primer
from django_kck.parameters import IntegerParameter, StringParameter
from django_kck.process import IntervalRefreshProcess, BaseProcess
from django_kck.hooks import PrePrimeHook, PostPrimeHook, PreSetHook, PostSetHook

logger = logging.getLogger(__name__)


class BaseSimplePrimer(Primer):
    compute_return = 'UNDEFINED COMPUTE RETURN'

    def compute(self, cache=None, hints=None):
        return self.compute_return


class StreetNamesPrimer(BaseSimplePrimer):
    key_base = 'street_names'
    prime_on_cache_miss = True
    compute_return = ['Oak', 'Maple', 'Spruce']


class NoExpirePrimer(BaseSimplePrimer):
    key_base = 'no_expire'
    compute_return = 'NO_EXPIRE_PRIMER'


class HardExpirePrimer(BaseSimplePrimer):
    key_base = 'hard_expire'
    hard_expire_seconds = 30
    compute_return = 'HARD_EXPIRE_PRIMER'


class SoftExpirePrimer(BaseSimplePrimer):
    key_base = 'soft_expire'
    soft_expire_seconds = 60
    compute_return = 'SOFT_EXPIRE_PRIMER'


class BothHardAndSoftExpirePrimer(BaseSimplePrimer):
    key_base = 'hard_and_soft_expire'
    soft_expire_seconds = 15
    hard_expire_seconds = 45
    compute_return = 'HARD_AND_SOFT_EXPIRE_PRIMER'


def mockable_function(*args, **kwargs):
    logger.info('mockable function called, unmocked')


def mockable_function_caller(*args, **kwargs):
    logger.info('mockable function caller called')
    from app_with_primers1.primers import mockable_function as f
    f(*args, **kwargs)


class MockableComputePrimer(Primer):
    key_base = 'mockable_compute'

    def compute(self, cache=None, hints=None):
        return mockable_function_caller()


class OnePreSetHookPrimer(BaseSimplePrimer):
    key_base = 'onepresethook'
    # hooks = {"pre-set": mockable_function_caller}
    hooks = (
        BaseSimplePrimer.hooks +
        (PreSetHook(mockable_function_caller),))

    compute_return = 'ONE_PRESET_HOOK'


class TwoPreSetHooksPrimer(BaseSimplePrimer):
    key_base = 'twopresethooks'
    # hooks = {"pre-set": [mockable_function_caller, mockable_function_caller]}
    hooks = (
        BaseSimplePrimer.hooks +
        (PreSetHook(mockable_function_caller), PreSetHook(mockable_function_caller)))
    compute_return = 'TWO_PRESET_HOOKS'


class OnePostSetHookPrimer(BaseSimplePrimer):
    key_base = 'onepostsethook'
    # hooks = {"post-set": mockable_function_caller}
    hooks = (
        BaseSimplePrimer.hooks +
        (PostSetHook(mockable_function_caller),))
    compute_return = 'ONE_POSTSET_HOOK'


class TwoPostSetHooksPrimer(BaseSimplePrimer):
    key_base = 'twopostsethooks'
    # hooks = {"post-set": [mockable_function_caller, mockable_function_caller]}
    hooks = (
        BaseSimplePrimer.hooks +
        (PostSetHook(mockable_function_caller), PostSetHook(mockable_function_caller)))
    compute_return = 'TWO_POSTSET_HOOKS'


class OnePrePrimeHookPrimer(BaseSimplePrimer):
    key_base = 'onepreprimehook'
    # hooks = {"pre-prime": mockable_function_caller}
    hooks = (
        BaseSimplePrimer.hooks +
        (PrePrimeHook(mockable_function_caller),))
    compute_return = 'ONE_PRE_PRIME_PRIMER'


class TwoPrePrimeHookPrimers(BaseSimplePrimer):
    key_base = 'twopreprimehooks'
    # hooks = {"pre-prime": [mockable_function_caller, mockable_function_caller]}
    hooks = (
        BaseSimplePrimer.hooks +
        (PrePrimeHook(mockable_function_caller), PrePrimeHook(mockable_function_caller)))
    compute_return = 'TWO_PRE_PRIME_PRIMERS'


class OnePostPrimeHookPrimer(BaseSimplePrimer):
    key_base = 'onepostprimehook'
    # hooks = {"post-prime": mockable_function_caller}
    hooks = (
        BaseSimplePrimer.hooks +
        (PostPrimeHook(mockable_function_caller),))
    compute_return = 'ONE_POST_PRIME_PRIMER'


class TwoPostPrimeHookPrimers(BaseSimplePrimer):
    key_base = 'twopostprimehooks'
    # hooks = {"post-prime": [mockable_function_caller, mockable_function_caller]}
    hooks = (
        BaseSimplePrimer.hooks +
        (PostPrimeHook(mockable_function_caller),))
    compute_return = 'TWO_POST_PRIME_PRIMERS'


class StringAndIntParametersPrimer(BaseSimplePrimer):
    key_base = 'string_and_int_parameters'
    parameters = [IntegerParameter('id'), StringParameter('name')]
    compute_return = 'STRING_AND_INT_PARAMS_PRIMER'


class SimpleCustomProcess(BaseProcess):
    def run(self, hints=None):
        mockable_function_caller()


class SimpleCustomProcessPrimer(BaseSimplePrimer):
    key_base = 'simple_custom_process'
    processes = [SimpleCustomProcess]
    compute_return = 'SIMPLE_CUSTOM_PROCESS_PRIMER'


class SimpleCustomIntervalRefreshProcess(IntervalRefreshProcess):
    def domain(self, hints=None):
        return ['interval_refresh/1', 'interval_refresh/123']


class IntervalRefreshPrimer(BaseSimplePrimer):
    key_base = 'interval_refresh'
    processes = [IntervalRefreshProcess]
    parameters = [IntegerParameter('id')]

    def compute(self):
        return str(datetime.datetime.utcnow())


class ExceptionThrowingPrimer(BaseSimplePrimer):
    key_base = 'exception_thrower'

    def compute(self, cache=None, hints=None):
        raise Exception('bah humbug')
