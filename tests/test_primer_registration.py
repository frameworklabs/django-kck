import pytest
from django_kck.test import BaseCacheTest


class TestPrimerRegistration(BaseCacheTest):
    def test_street_names_primer_exists(self, f_cache):
        cache = f_cache()
        assert cache.primer('street_names')

    def test_customer_names_primer_exists(self, f_cache):
        cache = f_cache()
        assert cache.primer('customer_names')
