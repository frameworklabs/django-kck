from unittest import mock

import datetime
import pytest
from django_kck.test import BaseCacheTest
from django_kck.models import RefreshQueue


class TestRefresh(BaseCacheTest):
    """test the refresh() method"""
    def test_unqueued_refresh_refreshes(self, f_cache):
        cache = f_cache()
        self.simulate_cache_entry(
            cache.cache_entry(
                key='street_names/1',
                value=['Robin', 'Sparrow', 'Hawk']))
        cache.refresh('street_names/1', queued=False)
        self.assert_current_cache_entry_value(cache, 'street_names/1', ['Oak', 'Maple', 'Spruce'])

    def test_queued_refresh_puts_item_on_queue(self, f_cache):
        assert RefreshQueue.objects.all().count() == 0
        cache = f_cache()
        self.simulate_cache_entry(
            cache.cache_entry(
                key='street_names/1',
                value=['Robin', 'Sparrow', 'Hawk']))
        cache.refresh('street_names/1', queued=True)
        self.assert_current_cache_entry_value(cache, 'street_names/1', ['Robin', 'Sparrow', 'Hawk'])
        assert RefreshQueue.objects.all().count() == 1


class TestRefreshRequests(BaseCacheTest):
    """test the refresh_requests() data gathering method"""
    def test_all(self, f_cache):
        # current refresh list
        cache = f_cache()
        refreshable_key_list = list(cache.refresh_requests().values_list('key', flat=True))
        assert not refreshable_key_list

        # request some refreshes
        key_list = ['street_names/1', 'street_names/2', 'customer_names/30']
        for key in key_list:
            cache.refresh(key, queued=True)

        # test that the refreshes are represented in the queue
        refreshable_key_list = list(cache.refresh_requests().values_list('key', flat=True))
        assert refreshable_key_list
        assert set(refreshable_key_list) == set(key_list)

    def test_primer_names(self, f_cache):
        # current refresh list
        cache = f_cache()
        refreshable_key_list = list(cache.refresh_requests().values_list('key', flat=True))
        assert not refreshable_key_list

        # request some refreshes
        key_list = ['street_names/1', 'street_names/2', 'customer_names/30']
        for key in key_list:
            cache.refresh(key, queued=True)

        # test that the refreshes are represented in the queue
        refreshable_key_list = list(cache.refresh_requests(primer_names=['street_names']).values_list('key', flat=True))
        assert refreshable_key_list
        assert set(refreshable_key_list) == {'street_names/1', 'street_names/2'}

    def claimant(self, f_cache):
        # current refresh list
        cache = f_cache()
        refreshable_key_list = list(cache.refresh_requests().values_list('key', flat=True))
        assert not refreshable_key_list

        # request some refreshes
        key_list = ['street_names/1', 'street_names/2', 'customer_names/30']
        for key in key_list:
            cache.refresh(key, queued=True)

        # claim a refresh
        cache.claim_refresh_requests('king_john', primer_names=['customer_names'])

        # test that the refreshes are represented in the queue
        refreshable_key_list = list(cache.refresh_requests(claimant='king_john').values_list('key', flat=True))
        assert refreshable_key_list
        assert set(refreshable_key_list) == {'customer_names/30'}


class TestRefreshQueueClaims(BaseCacheTest):
    """test the claim_refresh_requests() method"""
    def test_claim_all(self, f_cache):
        # current refresh list
        cache = f_cache()
        refreshable_key_list = list(cache.refresh_requests().values_list('key', flat=True))
        assert not refreshable_key_list

        # request some refreshes
        key_list = ['street_names/1', 'street_names/2', 'customer_names/30']
        for key in key_list:
            cache.refresh(key, queued=True)

        # claim them
        cache.claim_refresh_requests('king_john')

        # test that the refreshes are represented in the queue
        refreshable_key_list = list(cache.refresh_requests(claimant='king_john').values_list('key', flat=True))
        assert refreshable_key_list
        assert set(refreshable_key_list) == set(key_list)

    def test_claim_primer_names(self, f_cache):
        # current refresh list
        cache = f_cache()
        refreshable_key_list = list(cache.refresh_requests().values_list('key', flat=True))
        assert not refreshable_key_list

        # request some refreshes
        key_list = ['street_names/1', 'street_names/2', 'customer_names/30']
        for key in key_list:
            cache.refresh(key, queued=True)

        # claim a primer name
        cache.claim_refresh_requests('king_john', primer_names=['street_names'])

        # test that the refreshes are represented in the queue
        refreshable_key_list = list(cache.refresh_requests(claimant='king_john').values_list('key', flat=True))
        assert refreshable_key_list
        assert set(refreshable_key_list) == {'street_names/1', 'street_names/2'}

    def test_claim_limited_number(self, f_cache):
        # current refresh list
        cache = f_cache()
        refreshable_key_list = list(cache.refresh_requests().values_list('key', flat=True))
        assert not refreshable_key_list

        # request some refreshes
        key_list = ['street_names/1', 'street_names/2', 'customer_names/30']
        for key in key_list:
            cache.refresh(key, queued=True)

        # claim them
        cache.claim_refresh_requests('king_john', primer_names=['street_names'], max_requests=1)

        # test that the refreshes are represented in the queue
        refreshable_key_list = list(cache.refresh_requests(claimant='king_john').values_list('key', flat=True))
        assert len(refreshable_key_list) == 1
        assert refreshable_key_list[0] in ['street_names/1', 'street_names/2']


class TestPerformClaimedRefreshes(BaseCacheTest):
    """test perform_claimed_refreshes() method"""
    def test_perform_claimed_refreshes_refreshes(self, f_cache):
        assert RefreshQueue.objects.all().count() == 0
        cache = f_cache()
        self.simulate_cache_entry(
            cache.cache_entry(
                key='street_names/1',
                value=['Robin', 'Sparrow', 'Hawk']))
        cache.refresh('street_names/1', queued=True)
        cache.claim_refresh_requests('king_john', primer_names=['street_names'], max_requests=1)
        cache.perform_claimed_refreshes('king_john')
        self.assert_current_cache_entry_value(cache, 'street_names/1', ['Oak', 'Maple', 'Spruce'])

    def test_perform_claimed_refreshes_refreshes_when_one_raises_an_exception(self, f_cache):
        assert RefreshQueue.objects.all().count() == 0
        cache = f_cache()
        self.simulate_cache_entry(
            cache.cache_entry(
                key='street_names/1',
                value=['Robin', 'Sparrow', 'Hawk']))
        cache.refresh('exception_thrower', queued=True)
        cache.refresh('street_names/1', queued=True)
        cache.claim_refresh_requests('king_john', primer_names=['street_names', 'exception_thrower'])
        cache.perform_claimed_refreshes('king_john')
        self.assert_current_cache_entry_value(cache, 'street_names/1', ['Oak', 'Maple', 'Spruce'])


def f_raise_exception():
    raise Exception


class TestRefreshNotification(BaseCacheTest):
    """test that refresh sets the refresh cache entry"""

    key = 'mockable_compute'

    def test_uncached_refresh_updates_refresh_system_cache_entry(self, f_cache):
        with mock.patch('app_with_primers1.primers.mockable_function') as mock_mockable_function:

            # cause and forgive an exception that will interrupt the code during the prime
            # (and avoid the cleanup of the refreshing system key)
            mock_mockable_function.side_effect = f_raise_exception
            cache = f_cache()
            with pytest.raises(Exception):
                cache.refresh(self.key, queued=False)

            # test that the system key exists
            system_key = cache.build_system_key('refreshing', key=cache.build_key(self.key))
            cache_entry = cache.get(system_key)
            assert 'start' in cache_entry['value']
            assert cache_entry['value']['start'] < datetime.datetime.utcnow()

    def test_cached_refresh_does_not_update_refresh_system_cache_entry(self, f_cache):
        with mock.patch('app_with_primers1.primers.mockable_function') as mock_mockable_function:

            # cause an exception that will interrupt the code during the prime
            # (this doesn't happen since the cached refresh doesn't actually prime)
            mock_mockable_function.side_effect = f_raise_exception
            cache = f_cache()
            cache.refresh(self.key, queued=True)

            # test that the system key is not generated
            system_key = cache.build_system_key('refreshing', key=cache.build_key(self.key))
            with pytest.raises(KeyError):
                cache_entry = cache.get(system_key)


class TestIsRefreshingMethod(BaseCacheTest):
    """test that refresh sets the refresh cache entry"""

    key = 'mockable_compute'

    def test_uncached_refresh_updates_refresh_system_cache_entry(self, f_cache):
        with mock.patch('app_with_primers1.primers.mockable_function') as mock_mockable_function:

            # cause and forgive an exception that will interrupt the code during the prime
            # (and avoid the cleanup of the refreshing system key)
            mock_mockable_function.side_effect = f_raise_exception
            cache = f_cache()
            with pytest.raises(Exception):
                cache.refresh(self.key, queued=False)

            assert cache.is_refreshing(self.key)

    def test_cached_refresh_does_not_update_refresh_system_cache_entry(self, f_cache):
        with mock.patch('app_with_primers1.primers.mockable_function') as mock_mockable_function:

            # cause an exception that will interrupt the code during the prime
            # (this doesn't happen since the cached refresh doesn't actually prime)
            mock_mockable_function.side_effect = f_raise_exception
            cache = f_cache()
            cache.refresh(self.key, queued=True)

            assert not cache.is_refreshing(self.key)

