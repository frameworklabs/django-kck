from invoke import task
import os
import subprocess


def is_linux():
    uname_output = subprocess.Popen(["uname", "-a"],
                                    stdout=subprocess.PIPE).communicate()[0]
    if uname_output.decode('utf-8').startswith('Linux'):
        return True
    return False


@task
def startenv(c):
    c.run('docker-compose up')


@task
def stopenv(c):
    c.run('docker-compose down')


@task
def buildenv(c):
    c.run('docker-compose build')


@task
def shell(c):
    os.system('docker-compose run worker bash')


@task
def run_tests(c, filename=None, substring=None):

    dcrun = f'docker-compose run worker'
    path = filename or '/kck/django_kck/tests/'
    cmd = f'{dcrun} py.test {path} -v'
    if substring:
        cmd += f' -k {substring}'
    c.run(cmd)


@task
def clean(c):
    if is_linux():
        print("*** FIXING PERMISSIONS ***")
        dir_basename = os.path.basename(os.path.dirname(os.path.realpath(__file__)))
        c.run(f'sudo chown -R {os.geteuid()}.{os.getegid()} ../{dir_basename}')
    print("*** CLEANING UP REPO ***")
    c.run('rm -rf build dist')
    c.run('find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf')
    c.run('rm -rf django_kck.egg-info/')


@task(clean)
def build_package(c):
    c.run('python3 setup.py sdist bdist_wheel')
    c.run('rm -rf build')


@task
def upload_package(c):
    c.run('python3 -m twine upload --repository-url https://upload.pypi.org/legacy/ dist/*')
